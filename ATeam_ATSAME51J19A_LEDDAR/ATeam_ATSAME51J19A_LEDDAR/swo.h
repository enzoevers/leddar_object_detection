/*
 * swo.h
 *
 *  Created on: 10.10.2016
 *      Author: Erich Styger Local
 */

#ifndef SOURCES_SWO_H_
#define SOURCES_SWO_H_

#include <stdint.h>

void swo_print_char(char c, uint8_t portNumber);
void swo_print_string(const char *s, uint8_t portNumber);

void swo_init(uint32_t portBits, uint32_t cpuCoreFreqHz);


#endif /* SOURCES_SWO_H_ */