/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */

#include "driver_init.h"
#include <peripheral_clk_config.h>
#include <utils.h>
#include <hal_init.h>

struct can_async_descriptor CAN_0;
struct can_async_descriptor CAN_1;

void delay_driver_init(void)
{
	delay_init(SysTick);
}

void CAN_0_PORT_init(void)
{

	gpio_set_pin_function(CAN0_RX, PINMUX_PA23I_CAN0_RX);

	gpio_set_pin_function(CAN0_TX, PINMUX_PA22I_CAN0_TX);
}
/**
 * \brief CAN initialization function
 *
 * Enables CAN peripheral, clocks and initializes CAN driver
 */
void CAN_0_init(void)
{
	hri_mclk_set_AHBMASK_CAN0_bit(MCLK);
	hri_gclk_write_PCHCTRL_reg(GCLK, CAN0_GCLK_ID, CONF_GCLK_CAN0_SRC | (1 << GCLK_PCHCTRL_CHEN_Pos));
	can_async_init(&CAN_0, CAN0);
	CAN_0_PORT_init();
}

void CAN_1_PORT_init(void)
{

	gpio_set_pin_function(CAN1_RX, PINMUX_PB13H_CAN1_RX);

	gpio_set_pin_function(CAN1_TX, PINMUX_PB12H_CAN1_TX);
}
/**
 * \brief CAN initialization function
 *
 * Enables CAN peripheral, clocks and initializes CAN driver
 */
void CAN_1_init(void)
{
	hri_mclk_set_AHBMASK_CAN1_bit(MCLK);
	hri_gclk_write_PCHCTRL_reg(GCLK, CAN1_GCLK_ID, CONF_GCLK_CAN1_SRC | (1 << GCLK_PCHCTRL_CHEN_Pos));
	can_async_init(&CAN_1, CAN1);
	CAN_1_PORT_init();
}

void system_init(void)
{
	init_mcu();

	// GPIO on PB00

	gpio_set_pin_level(TestLED,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(TestLED, GPIO_DIRECTION_OUT);

	gpio_set_pin_function(TestLED, GPIO_PIN_FUNCTION_OFF);

	delay_driver_init();
	CAN_0_init();
	CAN_1_init();
}
