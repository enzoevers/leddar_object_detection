#include <atmel_start.h>
#include "swo.h"
#include <peripheral_clk_config.h>


bool led_state = true;

void blink_test() {
	gpio_set_pin_level(TestLED, true);
	delay_ms(1000);
	gpio_set_pin_level(TestLED, false);
	delay_ms(2000);
}
void CAN_0_tx_callback_own(struct can_async_descriptor *const descr)
{
	//printf("Message send on CAN0");
	(void)descr;
}
void CAN_0_rx_callback_own(struct can_async_descriptor *const descr)
{
	struct can_message msg;
	uint8_t            data[64];
	msg.data = data;
	can_async_read(descr, &msg);
	return;
}

void CAN_1_tx_callback_own(struct can_async_descriptor *const descr)
{
	(void)descr;
}
void CAN_1_rx_callback_own(struct can_async_descriptor *const descr)
{
	struct can_message msg;
	uint8_t            data[64];
	msg.data = data;
	can_async_read(descr, &msg);
	
	led_state = !led_state;
	gpio_set_pin_level(TestLED, led_state);
	
	//printf(descr->dev->context);
	//printf("Message received on CAN1");
	return;
}

void CAN0_to_CAN1_test() {
	
	 // CAN_1_rx_callback callback should be invoked after call
	 // can_async_set_filter and remote device send CAN Message with the same
	 // content as the filter.
	
	struct can_filter  filter;
	can_async_register_callback(&CAN_1, CAN_ASYNC_RX_CB, (FUNC_PTR)CAN_1_rx_callback_own);
	filter.id   = 0x45A;
	filter.mask = 0;
	can_async_set_filter(&CAN_1, 0, CAN_FMT_STDID, &filter);
	
	struct can_message msg;
	uint8_t            send_data[4];
	send_data[0] = 0x00;
	send_data[1] = 0x01;
	send_data[2] = 0x02;
	send_data[3] = 0x03;

	msg.id   = 0x45A;
	msg.type = CAN_TYPE_DATA;
	msg.data = send_data;
	msg.len  = 4;
	msg.fmt  = CAN_FMT_STDID;
	can_async_register_callback(&CAN_0, CAN_ASYNC_TX_CB, (FUNC_PTR)CAN_0_tx_callback_own);
	can_async_enable(&CAN_0);

	
	 // CAN_0_tx_callback callback should be invoked after call
	 // can_async_write, and remote device should recieve message with ID=0x45A
	 //
	can_async_write(&CAN_0, &msg);
}

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	swo_init(0x1, CONF_CPU_FREQUENCY);
	
	delay_ms(1000);
	//swo_print_string("HelloWorld.\r\n", 0);
	delay_ms(1000);

	/* Replace with your application code */
	while (1) {
		blink_test();
		swo_print_string("T\r\n", 0);
		delay_ms(1000);
	}
}
