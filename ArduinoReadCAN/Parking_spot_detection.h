#ifndef PARKING_SPOT_DETECTION_H_
#define PARKING_SPOT_DETECTION_H_

#include <Arduino.h>
#include <mcp_can.h>

enum parkingSpotDetectStates {
  searchingBackCar,
  carBack,
  parkingSpot,
  foundCarFront,
  carFront
};

#define trigPin 22
#define echoPin 21

#define searchParkingSpot_CANID 0x500
#define parkingSpotData_CANID   0x400
#define foundParkingSpot_CANID  0x300
#define carSpeed_CANID          0x600
#define distUltrasoneRequest_CANID 0x610
#define distUltrasoneData_CANID 0x620

const uint8_t foundFrontCarParkingSpot = 1;
const uint8_t foundFrontCarParkingSpotAndStopped = 2;

bool getSearchParkingSpotFlag();

void setSearchParkingSpot(uint8_t length, uint8_t buf[]);
void assignCarSpeed(uint8_t length, uint8_t buf[]);
void getDrivenDistance_cm(float* dist);
void parkingSpotMapping(MCP_CAN& CAN_Node);
void giveMeasuredDistToCar_CAN(MCP_CAN& CAN_Node, uint8_t length, uint8_t buf[]);

#endif PARKING_SPOT_DETECTION_H_
