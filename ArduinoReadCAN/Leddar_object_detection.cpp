#include "Leddar_object_detection.h"

const float cm_objectDistThreshold_x = 40; // Distance
const float cm_objectDistThreshold_y = 40; // Width

int numDetectedSegments = 0;

const int objxCAN_ID[NUM_OBJECTS_MAX] = {obj0CAN_ID, obj1CAN_ID, obj2CAN_ID, obj3CAN_ID};

float xmin = 10; //cm
float xmax = 4500; //cm

// active outputs: default values
// MIO_Bwd_A  initial settings
float Bwd_MIO_Datamode = 0;
float ID_Bwd_MIO = 0;
float Bwd_MIO_Distance = -250;
float Bwd_MIO_latdist = 0;
float Bwd_MIO_Range_Rate = 0;

// MIO_Bwd_L initial settings
float Bwd_MIO_L_Datamode = 0;
float ID_Bwd_MIO_L = 0;
float Bwd_MIO_L_Distance = -250;
float Bwd_MIO_L_latdist = 0;
float Bwd_MIO_L_Range_Rate = 0;

// MIO_Bwd_R  initial settings
float Bwd_MIO_R_Datamode = 0;
float ID_Bwd_MIO_R = 0;
float Bwd_MIO_R_Distance = -250;
float Bwd_MIO_R_latdist = 0;
float Bwd_MIO_R_Range_Rate = 0;

#define segmentDegree 3
#define segmentDegreeHalf (float)(segmentDegree / 2)
const uint8_t numLeddars = NUM_LEDDARS;
const uint8_t numSegments = NUM_SEGMENTS_PER_LEDDAR;

const uint8_t maxObjects = NUM_OBJECTS_MAX;
uint8_t detectedObjects = maxObjects; //0;

struct LeddarData   leddarData[NUM_LEDDARS][NUM_SEGMENTS_PER_LEDDAR];
struct objectStruct objects[NUM_OBJECTS_MAX];
struct pos          posFromMid[NUM_LEDDARS * NUM_SEGMENTS_PER_LEDDAR];
const struct pos invalidPos = { -65536, -65536};

struct leddarDataOffset leddarDataSegments[NUM_LEDDARS][NUM_SEGMENTS_PER_LEDDAR] = {
  {
    {0,  21 + segmentDegreeHalf, 21.943, cm_LeddarMiddleOffset_1},
    {1,  18 + segmentDegreeHalf, 20.049, cm_LeddarMiddleOffset_1},
    {2,  15 + segmentDegreeHalf, 20.160, cm_LeddarMiddleOffset_1},
    {3,  12 + segmentDegreeHalf, 19.630, cm_LeddarMiddleOffset_1},
    {4,   9 + segmentDegreeHalf, 19.893, cm_LeddarMiddleOffset_1},
    {5,   6 + segmentDegreeHalf, 20.792, cm_LeddarMiddleOffset_1},
    {6,   3 + segmentDegreeHalf, 20.885, cm_LeddarMiddleOffset_1},
    {7,   0 + segmentDegreeHalf, 18.743, cm_LeddarMiddleOffset_1},
    {8,   0 + segmentDegreeHalf, 20.955, cm_LeddarMiddleOffset_1},
    {9,   3 + segmentDegreeHalf, 21.475, cm_LeddarMiddleOffset_1},
    {10,  6 + segmentDegreeHalf, 22.065, cm_LeddarMiddleOffset_1},
    {11,  9 + segmentDegreeHalf, 21.384, cm_LeddarMiddleOffset_1},
    {12, 12 + segmentDegreeHalf, 19.566, cm_LeddarMiddleOffset_1},
    {13, 15 + segmentDegreeHalf, 20.751, cm_LeddarMiddleOffset_1},
    {14, 18 + segmentDegreeHalf, 23.195, cm_LeddarMiddleOffset_1},
    {15, 21 + segmentDegreeHalf, 30.443, cm_LeddarMiddleOffset_1}
  },
  {
    {0,  21 + segmentDegreeHalf, 21.943, cm_LeddarMiddleOffset_2},
    {1,  18 + segmentDegreeHalf, 20.049, cm_LeddarMiddleOffset_2},
    {2,  15 + segmentDegreeHalf, 20.160, cm_LeddarMiddleOffset_2},
    {3,  12 + segmentDegreeHalf, 19.630, cm_LeddarMiddleOffset_2},
    {4,   9 + segmentDegreeHalf, 19.893, cm_LeddarMiddleOffset_2},
    {5,   6 + segmentDegreeHalf, 20.792, cm_LeddarMiddleOffset_2},
    {6,   3 + segmentDegreeHalf, 20.885, cm_LeddarMiddleOffset_2},
    {7,   0 + segmentDegreeHalf, 18.743, cm_LeddarMiddleOffset_2},
    {8,   0 + segmentDegreeHalf, 20.955, cm_LeddarMiddleOffset_2},
    {9,   3 + segmentDegreeHalf, 21.475, cm_LeddarMiddleOffset_2},
    {10,  6 + segmentDegreeHalf, 22.065, cm_LeddarMiddleOffset_2},
    {11,  9 + segmentDegreeHalf, 21.384, cm_LeddarMiddleOffset_2},
    {12, 12 + segmentDegreeHalf, 19.566, cm_LeddarMiddleOffset_2},
    {13, 15 + segmentDegreeHalf, 20.751, cm_LeddarMiddleOffset_2},
    {14, 18 + segmentDegreeHalf, 23.195, cm_LeddarMiddleOffset_2},
    {15, 21 + segmentDegreeHalf, 30.443, cm_LeddarMiddleOffset_2}
  }
};

void initLeddarData() {
  for (int i = 0; i < (NUM_LEDDARS * NUM_SEGMENTS_PER_LEDDAR); i++) {
    posFromMid[i] = invalidPos;
  }
}

void initObjectArray() {
  for (int i = 0; i < maxObjects; i++) {
    for (int j = 0; j < numLeddars * numSegments; j++) {
      objects[i].points[j] = invalidPos;
    }

    objects[i].closestPoint = {65535, 65535, 65535};
    objects[i].objWidth_cm = 0;
    objects[i].objPos = invalidPos;
  }
}

void printData() {
  for (int8_t i = 0; i < numLeddars * numSegments; i++) {
    Serial.print("x: ");
    Serial.print(posFromMid[i].x);
    Serial.print(" | Y: ");
    Serial.println(posFromMid[i].y);
  }
  Serial.println();

  for (int i = 0; i < detectedObjects; i++) {
    //for (int i = 0; i < maxObjects; i++) {
    Serial.print("objects: ");
    Serial.print(objects[i].id);
    Serial.print(" | X: ");
    Serial.print(objects[i].closestPoint.Rx);
    Serial.print(" | Y: ");
    Serial.println(objects[i].closestPoint.Ry);
    /*Serial.print(" | width: ");
      Serial.print(objects[i].objWidth_cm);
      Serial.print(" | start pos (x, y): (");
      Serial.print(objects[i].objPos.x);
      Serial.print(", ");
      Serial.print(objects[i].objPos.y);
      Serial.println(")");*/
  }
  Serial.println();
}

void parseDetectionData(int ID, uint8_t length, uint8_t buf[])
{
  int index_n = 0;
  int index_n1 = 0;
  float myRadians = 0;
  int leddarID = -1;

  switch (ID) {
    case LeddarTx_1:
      leddarID = 0;
      break;
    case LeddarTx_2:
      leddarID = 1;
      break;
    default:
      leddarID = -1;
      break;
  }

  if (leddarID == -1) {
    return;
  }

  //====================
  // Even segment
  //====================

  // Read CAN data
  index_n = (buf[3] & 0xF0) >> 4;
  leddarData[leddarID][index_n].segment = index_n;
  leddarData[leddarID][index_n].distance = buf[0] | buf[1] << 8;
  leddarData[leddarID][index_n].amplitude = (buf[2] | ((buf[3] & 0xF) << 8)) / 4;

  // Apply offset
  //leddarData[leddarID][index_n].distance -= leddarDataSegments[leddarID][index_n].dist_offset;

  // Calculate X and Y
  myRadians = deg2rad(leddarDataSegments[leddarID][index_n].degreeMiddle);
  leddarData[leddarID][index_n].x_from_mid_leddar = cos(myRadians) * leddarData[leddarID][index_n].distance;
  leddarData[leddarID][index_n].y_from_mid_leddar = sin(myRadians) * leddarData[leddarID][index_n].distance;

  // Check if to the left of right from the middle of the leddar
  if (index_n < (numSegments / 2) - 1) {
    leddarData[leddarID][index_n].y_from_mid_leddar *= -1;
  }

  /*
    if (leddarData[leddarID][index_n].x_from_mid_leddar < xmin || leddarData[leddarID][index_n].x_from_mid_leddar > xmax) {
    leddarData[leddarID][index_n].x_from_mid_leddar = invalidPos.x;
    leddarData[leddarID][index_n].y_from_mid_leddar = invalidPos.y;
    }
  */

  //====================
  // Uneven segment
  //====================

  // Read CAN data
  index_n1 = (buf[7] & 0xF0) >> 4;
  leddarData[leddarID][index_n1].segment = index_n1;
  leddarData[leddarID][index_n1].distance = buf[4] | buf[5] << 8;
  leddarData[leddarID][index_n1].amplitude = (buf[6] | ((buf[7] & 0xF) << 8)) / 4;

  // Apply offset
  //leddarData[leddarID][index_n1].distance -= leddarDataSegments[leddarID][index_n1].dist_offset;

  // Calculate X and Y
  myRadians = deg2rad(leddarDataSegments[leddarID][index_n1].degreeMiddle);
  leddarData[leddarID][index_n1].x_from_mid_leddar = cos(myRadians) * leddarData[leddarID][index_n1].distance;
  leddarData[leddarID][index_n1].y_from_mid_leddar = sin(myRadians) * leddarData[leddarID][index_n1].distance;

  // Check if to the left of right from the middle of the leddar
  // Right half of the middle is positve y
  if (index_n1 < (numSegments / 2) - 1) {
    leddarData[leddarID][index_n1].y_from_mid_leddar *= -1;
  }

  /*
    if (leddarData[leddarID][index_n1].x_from_mid_leddar < xmin || leddarData[leddarID][index_n1].x_from_mid_leddar > xmax) {
      leddarData[leddarID][index_n1].x_from_mid_leddar = invalidPos.x;
      leddarData[leddarID][index_n1].y_from_mid_leddar = invalidPos.y;
    }
  */

  //===================================
  // Put all data in a signel array
  //===================================
  posFromMid[(leddarID * numSegments) + index_n].x = leddarData[leddarID][index_n].x_from_mid_leddar;
  posFromMid[(leddarID * numSegments) + index_n].y = /*leddarDataSegments[leddarID][index_n].y_offset*/ + leddarData[leddarID][index_n].y_from_mid_leddar;

  posFromMid[(leddarID * numSegments) + index_n1].x = leddarData[leddarID][index_n1].x_from_mid_leddar;
  posFromMid[(leddarID * numSegments) + index_n1].y = /*leddarDataSegments[leddarID][index_n1].y_offset +*/ leddarData[leddarID][index_n1].y_from_mid_leddar;


}

void sortY_ascending() {

  int numVals = numLeddars * numSegments;
  int numValsToSort = numVals;

  while (numValsToSort > 1) {

    double higestY = abs(invalidPos.y);
    float margin = 1;
    int indexHighestY = -1;

    for (int i = numVals - numValsToSort; i < numVals; i++) {
      float currentY = posFromMid[i].y;
      if (currentY != invalidPos.y) {
        if (currentY < higestY - margin && currentY < higestY + margin) {
          higestY = currentY;
          indexHighestY = i;
        }
      }
    }

    if (indexHighestY != -1) {
      struct pos tmp_posFromMid = posFromMid[numVals - numValsToSort];
      posFromMid[numVals - numValsToSort] = posFromMid[indexHighestY];
      posFromMid[indexHighestY] = tmp_posFromMid;
    }

    numValsToSort--;

  }
}

void sortX_ascending() {

  int numVals = numLeddars * numSegments;
  int numValsToSort = numVals;

  while (numValsToSort > 1) {

    double higestX = abs(invalidPos.x);
    float margin = 1;
    int indexHighestX = -1;

    for (int i = numVals - numValsToSort; i < numVals; i++) {
      float currentX = posFromMid[i].x;
      if (currentX != invalidPos.x) {
        if (currentX < higestX - margin && currentX < higestX + margin) {
          higestX = currentX;
          indexHighestX = i;
        }
      }
    }

    if (indexHighestX != -1) {
      struct pos tmp_posFromMid = posFromMid[numVals - numValsToSort];
      posFromMid[numVals - numValsToSort] = posFromMid[indexHighestX];
      posFromMid[indexHighestX] = tmp_posFromMid;
    }

    numValsToSort--;

  }
}

void sortObjects_ascending() {
  int numObjects = maxObjects;
  int numObjectsToSort = numObjects;

  while (numObjectsToSort > 1) {

    double higestRx = abs(invalidPos.y);
    float margin = 1;
    int indexHighestRx = -1;

    for (int i = numObjects - numObjectsToSort; i < numObjects; i++) {
      float currentObjRx = objects[i].closestPoint.Rx;
      if (currentObjRx != invalidPos.x) {
        if (currentObjRx < higestRx - margin && currentObjRx < higestRx + margin) {
          higestRx = currentObjRx;
          indexHighestRx = i;
        }
      }
    }

    if (indexHighestRx != -1) {
      struct objectStruct tmp_object = objects[numObjects - numObjectsToSort];
      objects[numObjects - numObjectsToSort] = objects[indexHighestRx];
      objects[indexHighestRx] = tmp_object;
    }

    numObjectsToSort--;

  }

  /*
     Re-asign the ids
  */
  for (int i = 0; i < numObjects; i++) {
    objects[i].id = i;
  }
}

void parseAdditionalInformation(int ID, uint8_t length, uint8_t buf[])
{
  numDetectedSegments = buf[0];
  /*
    Serial.println("***\tStatus Message\t***");

    if (length < 8)
    {
    Serial.println("ERROR Information data not statisfying: parseAdditionalInformation");
    return;
    }

    Serial.print("Detections:\t");
    Serial.println(buf[0]);

    Serial.print("LED Power:\t");
    Serial.println(buf[2]);

    Serial.print("Acquisition:\t");
    Serial.println(buf[3] == 0x01 ? "true" : "false");

    Serial.print("Timestamp:\t");
    unsigned long timeStamp = buf[4] | (buf[5] << 8); //| (buf[6] << 8*2) | (buf[7] << 8*3);
    Serial.println(timeStamp);
    Serial.println();
  */
}

void backMIO_simple() {

  // Make sure the closest point is on index 0.
  sortX_ascending();

  int objCount = numDetectedSegments;
  if (objCount > maxObjects) {
    objCount = maxObjects;
  }

  int k = 0;

  for (int i = 0; i < maxObjects; i++) {

    struct pos curPos = posFromMid[k];

    if (curPos.x != 0 && curPos.x != invalidPos.x) {
      //Serial.println(curPos.x);
      objects[i].id = i;
      objects[i].points[0] = curPos;
      objects[i].closestPoint.Rx = curPos.x;
      objects[i].closestPoint.Ry = curPos.y;
      objects[i].objWidth_cm = 2 * (atan((segmentDegreeHalf / 180) * M_PI) * objects[i].closestPoint.Rx);
      objects[i].objPos = curPos;
    }
    else {
      objCount--;
      i--;
    }

    k++;
  }

  //Serial.println();

  //Serial.println(objects[0].closestPoint.Rx);
  //Serial.println();

  printData();
  //Serial.println(detectedObjects);

  //detectedObjects = maxObjects;

  //printData();
}

void backMIO() {
  sortY_ascending();

  /*
    for (int8_t i = 0; i < numLeddars * numSegments; i++) {
    Serial.print("x: ");
    Serial.print(posFromMid[i].x);
    Serial.print(" | Y: ");
    Serial.println(posFromMid[i].y);
    }
    Serial.println();
  */


  int8_t currentObj = 0;
  uint8_t numPointsForObj = 0;
  initObjectArray();

  bool y_threshold_met = false;
  bool x_threshold_met = false;

  uint16_t closestX = xmax;

  uint8_t furthestObjID = 255;

  bool searchingClosestObj = false;

  objects[0].points[0].x = posFromMid[0].x;
  objects[0].points[0].y = posFromMid[0].y;
  objects[0].objPos = posFromMid[0];
  numPointsForObj = 1;
  detectedObjects = 1;

  bool firstRound = true;

  for (int8_t i = 1; i < (numLeddars * numSegments); i++) {

    if (posFromMid[i].x > (invalidPos.x + 10) && posFromMid[i].y > (invalidPos.y + 10)) {

      y_threshold_met = abs(posFromMid[i].y - objects[currentObj].points[numPointsForObj - 1].y) > cm_objectDistThreshold_y;
      x_threshold_met = abs(posFromMid[i].x - posFromMid[i - 1].x) > cm_objectDistThreshold_x;

      /*
        Detected new object
      */
      if (x_threshold_met && y_threshold_met) {
        // A table with showing distance and segment width:
        // Basically every meter the width increased by ~5cm
        //
        // 2*(atan(deg(1.5)) * distance)
        //
        // Distance | segment width
        // (meter)  | (meter)
        // ---------|--------------
        //  1       | 0.05
        //  2       | 0.10
        //  3       | 0.16
        //  4       | 0.21
        //  5       | 0.26
        //  6       | 0.31
        //  7       | 0.37
        //  8       | 0.42
        //  9       | 0.57
        //  10      | 0.52
        if (numPointsForObj == 1) {
          objects[currentObj].objWidth_cm = 2 * (atan((segmentDegreeHalf / 180) * M_PI) * objects[currentObj].closestPoint.Rx);
        }

        if (searchingClosestObj == false) {
          currentObj++;
          detectedObjects++;
        }

        if (detectedObjects >= maxObjects) {
          detectedObjects = maxObjects;
        }

        if (currentObj > maxObjects) {
          currentObj = maxObjects;
        }

        /*
           Check if a new object should be added.
           Also check if there is an even closer object when all placeses in the object array are
           filled but there are still unprocessed segments.
        */
        if (currentObj == maxObjects || searchingClosestObj == true ) {
          if (posFromMid[i].x < objects[furthestObjID].closestPoint.Rx) {

            objects[furthestObjID].closestPoint.Rx = posFromMid[i].x;
            objects[furthestObjID].closestPoint.Ry = posFromMid[i].y;

            objects[furthestObjID].points[0].x = posFromMid[0].x;
            objects[furthestObjID].points[0].y = posFromMid[0].y;
            for (int8_t s = 1; s < numLeddars * numSegments; s++) {
              objects[furthestObjID].points[s] = invalidPos;
            }

            objects[furthestObjID].objWidth_cm = 0;

            furthestObjID = 0;
            for (int b = 0; b < maxObjects; b++) {
              if (objects[b].closestPoint.Rx > objects[furthestObjID].closestPoint.Rx) {
                furthestObjID = b;
              }
            }
            currentObj = furthestObjID;

            searchingClosestObj = true;
          }
        }
        else {
          objects[currentObj].closestPoint.Rx = xmax;
        }

        numPointsForObj = 0;
      }

      /*
         Assign points to object properties
      */
      if (currentObj >= 0 && currentObj < maxObjects) {
        if (posFromMid[i].x < xmax && posFromMid[i].x > 0) {

          objects[currentObj].points[numPointsForObj] = posFromMid[i];

          if (numPointsForObj > 0) {
            objects[currentObj].objWidth_cm += posFromMid[i].y - objects[currentObj].points[numPointsForObj - 1].y;
          }
          else if (numPointsForObj == 0) {
            objects[currentObj].objPos = posFromMid[i];
          }

          if (posFromMid[i].x < objects[currentObj].closestPoint.Rx) {
            objects[currentObj].closestPoint.Rx = posFromMid[i].x;
            objects[currentObj].closestPoint.Ry = posFromMid[i].y;
          }

          if (furthestObjID == 255) {
            furthestObjID = 0;
          }
          else {
            if (objects[currentObj].closestPoint.Rx > objects[furthestObjID].closestPoint.Rx) {
              furthestObjID = currentObj;
            }
          }

          numPointsForObj++;
        }
      }
    }
  }

  if (numPointsForObj == 1) {
    objects[currentObj].objWidth_cm = 2 * (atan((segmentDegreeHalf / 180) * M_PI) * objects[currentObj].closestPoint.Rx);
  }

  sortObjects_ascending();

  if (detectedObjects > maxObjects) {
    detectedObjects = maxObjects;
  }

  /*
    for (int i = 0; i < detectedObjects; i++) {
      Serial.print("objects: ");
      Serial.print(objects[i].id);
      Serial.print(" | X: ");
      Serial.print(objects[i].closestPoint.Rx);
      Serial.print(" | Y: ");
      Serial.print(objects[i].closestPoint.Ry);
      Serial.print(" | width: ");
      Serial.print(objects[i].objWidth_cm);
      Serial.print(" | start pos (x, y): (");
      Serial.print(objects[i].objPos.x);
      Serial.print(", ");
      Serial.print(objects[i].objPos.y);
      Serial.println(")");
    }
    Serial.println();
  */

}

void sendMIOsOverCAN(MCP_CAN& CAN_Node) {

  /*
     Send the number of detected objects to dSPACE
  */
  byte detectedObjects_byte = (byte)detectedObjects;

  byte sndStat = CAN_Node.sendMsgBuf(objNumCAN_ID, 1, &detectedObjects_byte);
  if (sndStat == CAN_OK) {
    //Serial.println("Message Sent Successfully!");
  } else {
    //Serial.println("Error Sending Message...");
  }

  //Serial.println(detectedObjects);

  if (detectedObjects > maxObjects) {
    detectedObjects = detectedObjects;
  }

  for (uint8_t i = 0; i < detectedObjects; i++) {

    uint16_t Rx_int    = (uint16_t)objects[i].closestPoint.Rx;
    int16_t  Ry_int    = (int16_t) objects[i].closestPoint.Ry;
    uint16_t width_int = (uint16_t)objects[i].objWidth_cm;
    uint16_t posx_int  = (uint16_t)objects[i].objPos.x;
    int16_t  posy_int  = (int16_t) objects[i].objPos.y;

    byte Rx_H = (Rx_int & (0xFF << 8)) >> 8;
    byte Rx_L = Rx_int & 0xFF;
    byte Ry_H = (Ry_int & (0xFF << 8)) >> 8;
    byte Ry_L = Ry_int & 0xFF;
    byte width_H = (width_int & (0xFF << 8)) >> 8;
    byte width_L = width_int & 0xFF;
    byte posx_H = (posx_int & (0xFF << 8)) >> 8;
    byte posx_L = posx_int & 0xFF;
    byte posy_H = (posy_int & (0xFF << 8)) >> 8;
    byte posy_L = posy_int & 0xFF;

    byte id = (byte)objects[i].id;


    byte data_1[7] = {id, Rx_H, Rx_L, Ry_H, Ry_L, width_H, width_L};

    byte sndStat = CAN_Node.sendMsgBuf(objxCAN_ID[i], 7, data_1);
    if (sndStat == CAN_OK) {
      //Serial.println("Message data_1 Sent Successfully!");
    } else {
      //Serial.println("Error Sending Message data_1...");
    }

    //byte data_2[4] = {posx_H, posx_L, posy_H, posy_L};
    //sndStat = CAN_Node.sendMsgBuf(objCAN_ID, 4, data_2);
    //if (sndStat == CAN_OK) {
    //Serial.println("Message data_2 Sent Successfully!");
    //} else {
    //Serial.println("Error Sending Message data_2...");
    //}

  }

  //Serial.println();

  //detectedObjects = 0;
}

float deg2rad(float deg) {
  return (deg * M_PI) / 180;
}
