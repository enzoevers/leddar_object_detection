#ifndef LEDDAR_OBJECT_DETECTION_H_
#define LEDDAR_OBJECT_DETECTION_H_

#include <Arduino.h>
#include <mcp_can.h>

#define NUM_OBJECTS_MAX 4
#define NUM_LEDDARS 2
#define NUM_SEGMENTS_PER_LEDDAR 16

// Left
#define LeddarTx_1 0x750
#define LeddarRx_1 0x740
#define cm_LeddarMiddleOffset_1 25

// Right
#define LeddarTx_2 0x752
#define LeddarRx_2 0x741
#define cm_LeddarMiddleOffset_2 -25

#define objNumCAN_ID  0x769
#define obj0CAN_ID    0x770
#define obj1CAN_ID    0x771
#define obj2CAN_ID    0x772
#define obj3CAN_ID    0x773
#define xyCAN_ID                     0x760

struct leddarDataOffset {
  uint8_t segment;
  float degreeMiddle;
  float dist_offset;
  int8_t y_offset;
};

struct LeddarData {
  uint8_t segment;
  uint16_t distance;
  uint16_t amplitude;
  float x_from_mid_leddar;
  float y_from_mid_leddar;
};

struct pos {
  float x;
  float y;
};

struct fusedLeddarData {
  float Rx;
  float Ry;
  float Vx;
};

struct objectStruct {
  /*
     id: the objects number, A lower id means a closer object. An id of 0 is the closest object.
     points: all the measurement points belonging to this object.
     closestPoint: the x and y of the closes object.
     objWidth_cm: the width of the object measured from the most left (looking from the back of the car to the object) y measurement point.
     objPos: the most left x and y position relative from the middle of the back of the car
  */
  int id;
  struct pos points[NUM_LEDDARS * NUM_SEGMENTS_PER_LEDDAR];
  struct fusedLeddarData closestPoint;
  float objWidth_cm;
  struct pos objPos;
};


void initLeddarData();
void initObjectArray();
void printData();
void parseDetectionData(int ID, uint8_t length, uint8_t buf[]);
void sortY_ascending();
void sortObjects_ascending();
void parseAdditionalInformation(int ID, uint8_t length, uint8_t buf[]);
void backMIO_simple();
void backMIO();
void sendMIOsOverCAN(MCP_CAN& CAN_Node);

float deg2rad(float deg);

#endif LEDDAR_OBJECT_DETECTION_H_
