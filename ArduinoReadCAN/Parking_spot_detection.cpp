#include "Parking_spot_detection.h"


bool afterParkingSpot = false;
bool onParkingSpot = false;

float parkingLength_cm = 0;
float lengthBesidesFrontCar_cm = 0;
float lengthBesidesBackCar_cm = 0;
const float lengthThreshold_cm = 10;

parkingSpotDetectStates state = searchingBackCar;

int16_t carSpeed_cmps = 0;
bool searchParkingSpot = false;


void resetParkingSpotData() {
  afterParkingSpot = false;
  onParkingSpot = false;

  parkingLength_cm = 0;
  lengthBesidesFrontCar_cm = 0;
  lengthBesidesBackCar_cm = 0;

  state = searchingBackCar;

  searchParkingSpot = false;
}

bool getSearchParkingSpotFlag() {
  return searchParkingSpot;
}

void setSearchParkingSpot(uint8_t length, uint8_t buf[]) {
  if (length < 1)
  {
    Serial.println("ERROR Information data not statisfying: setSearchParkingSpot");
    return;
  }

  searchParkingSpot = buf[0];
  Serial.print("searchParkingSpot: ");
  Serial.println(searchParkingSpot);

  if (searchParkingSpot == false) {
    resetParkingSpotData();
  }
}

void assignCarSpeed(uint8_t length, uint8_t buf[]) {
  if (length < 2)
  {
    Serial.println("ERROR Information data not statisfying: assignCarSpeed");
    return;
  }

  carSpeed_cmps = buf[0] | (buf[1] << 8);
  //Serial.print("carSpeed_cmps: ");
  //Serial.println(carSpeed_cmps);
}

void getDrivenDistance_cm(float* dist) {
  static unsigned long last_time = millis();
  unsigned long current_time = millis();
  unsigned long dt = current_time - last_time;

  Serial.print("dt: ");
  Serial.println(dt);
  Serial.print("carSpeed_cmps: ");
  Serial.println(carSpeed_cmps);
  Serial.print("dist before: ");
  Serial.println(*dist);

  float distDriven = ((carSpeed_cmps) / 1000.0) * dt; // Make it cm/ms

  *dist += distDriven;
  last_time = current_time;

  Serial.print("dist after: ");
  Serial.println(*dist);
}

float measureDistance_cm() {
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  long duration = pulseIn(echoPin, HIGH);
  /*
     Speed of sound: 0.034 cm/s
     time = distance/speed -> distance = time * speed
     distanceActual = (time * speed) / 2  (x/2 because of reflacting back from the object)
     https://howtomechatronics.com/tutorials/arduino/ultrasonic-sensor-hc-sr04/
  */
  float distanceCm = duration * 0.03448276 / 2;

  return distanceCm;
}

void giveMeasuredDistToCar_CAN(MCP_CAN& CAN_Node, uint8_t length, uint8_t buf[]) {
  if (length < 1)
  {
    Serial.println("ERROR Information data not statisfying: setSearchParkingSpot");
    return;
  }

  int request = buf[0];
  uint16_t distUltrasone_cm = 0;

  if (request == 1) {
  //if(true) {  
    distUltrasone_cm = (int)measureDistance_cm();

    uint16_t distUltrasone_cm_uint16 = (uint16_t)distUltrasone_cm;
    byte distUltrasone_cm_uint16_H = (distUltrasone_cm_uint16 & 0xFF << 8) >> 8;
    byte distUltrasone_cm_uint16_L = distUltrasone_cm_uint16 & 0xFF;

    byte data_0[2] = {distUltrasone_cm_uint16_H, distUltrasone_cm_uint16_L};
    byte sndStat = CAN_Node.sendMsgBuf(distUltrasoneData_CANID, 2, data_0);
    if (sndStat == CAN_OK) {
      //Serial.println("parkingSpotData_CANID: Message data_1 Sent Successfully!");
    } else {
      //Serial.println("Error Sending Message data_1...");
    }
  }
}

void parkingSpotMapping(MCP_CAN& CAN_Node) {
  /*
      XY array should be sorted ascending on the Y values.
  */
  static const float thresholdMostLeftYs_cm = 5;

  static const float sideCarDistThreshold_min = 120;
  static const float sideCarDistThreshold_max = 250;

  static const float parkSpotDistThreshold_min = 230;
  static const float parkSpotDistThreshold_max = 50000;

  /*
     This is only for the temporary setup where the leddar will be places on the side.
  */
  //const float y0 = leddarData[0][7].distance;
  //const float y1 = leddarData[0][8].distance;

  float distanceCm = measureDistance_cm();

  Serial.print("distanceCm: ");
  Serial.println(distanceCm);

  uint16_t distanceCm_uint16 = (uint16_t)distanceCm;
  byte distanceCm_uint16_H = (distanceCm_uint16 & 0xFF << 8) >> 8;
  byte distanceCm_uint16_L = distanceCm_uint16 & 0xFF;

  byte carSpeed_cmps_uint16_H = (carSpeed_cmps & 0xFF << 8) >> 8;
  byte carSpeed_cmps_uint16_L = carSpeed_cmps & 0xFF;

  byte data_1[5] = {distanceCm_uint16_H, distanceCm_uint16_L, carSpeed_cmps_uint16_H, carSpeed_cmps_uint16_L, 0};
  byte sndStat = CAN_Node.sendMsgBuf(parkingSpotData_CANID, 5, data_1);
  if (sndStat == CAN_OK) {
    Serial.println("parkingSpotData_CANID: Message data_1 Sent Successfully!");
  } else {
    Serial.println("Error Sending Message data_1...");
  }

  float avg_FrontCarGap = 0;
  int avgCounter = 0;

  bool besidesCarDist = distanceCm > sideCarDistThreshold_min && distanceCm < sideCarDistThreshold_max;
  bool besidesParkingSpotDist = distanceCm > parkSpotDistThreshold_min;// && distanceCm < parkSpotDistThreshold_max;

  bool inMarginThreshold = false;

  switch (state) {
    case searchingBackCar:
      if (besidesCarDist) {
        state = carBack;
      }
      break;
    case carBack:
      getDrivenDistance_cm(&lengthBesidesBackCar_cm);
      //inMarginThreshold = lengthBesidesBackCar_cm > (200 + lengthThreshold_cm) && lengthBesidesBackCar_cm > (200 - lengthThreshold_cm);
      inMarginThreshold = lengthBesidesBackCar_cm > 150;

      Serial.print("lengthBesidesBackCar_cm: ");
      Serial.println(lengthBesidesBackCar_cm);

      if (inMarginThreshold && besidesParkingSpotDist) {
        state = parkingSpot;
      }
      break;

    case parkingSpot:
      getDrivenDistance_cm(&parkingLength_cm);
      //inMarginThreshold = parkingLength_cm < (600 + lengthThreshold_cm) && parkingLength_cm > (600 - lengthThreshold_cm);
      inMarginThreshold = parkingLength_cm > 650;

      Serial.print("parkingLength_cm: ");
      Serial.println(parkingLength_cm);

      if (inMarginThreshold && besidesCarDist) {
        state = foundCarFront;
      }
      else if (inMarginThreshold == false && besidesCarDist == true) {
        resetParkingSpotData();
      }
      else {
        // nothing
      }
      break;

    case foundCarFront: { // Variable declerations in a switch/case should be in a scope {} in C++.

        byte foundFrontCarParkingSpot_byte = (byte)foundFrontCarParkingSpot;

        byte data_0[1] = {1};
        byte sndStat = CAN_Node.sendMsgBuf(foundParkingSpot_CANID, 1, data_0);
        if (sndStat == CAN_OK) {
          Serial.println("parkingSpotData_CANID: Message data_1 Sent Successfully!");
        } else {
          Serial.println("Error Sending Message data_1...");
        }

        state = carFront;
      }

      break;

    case carFront:
      getDrivenDistance_cm(&lengthBesidesFrontCar_cm);
      //inMarginThreshold = lengthBesidesFrontCar_cm < (100 + lengthThreshold_cm) && lengthBesidesFrontCar_cm > (100 - lengthThreshold_cm);
      inMarginThreshold = carSpeed_cmps < 0.05;

      Serial.print("lengthBesidesFrontCar_cm: ");
      Serial.println(lengthBesidesFrontCar_cm);

      avgCounter++;
      avg_FrontCarGap += distanceCm;
      avg_FrontCarGap /= avgCounter;

      Serial.print("avg_FrontCarGap: ");
      Serial.println(avg_FrontCarGap);

      if (inMarginThreshold) {

        Serial.println();
        Serial.print("parkingLength_cm: ");
        Serial.println(parkingLength_cm);
        Serial.print("lengthBesidesFrontCar_cm: ");
        Serial.println(lengthBesidesFrontCar_cm);
        Serial.print("avg_FrontCarGap: ");
        Serial.println(avg_FrontCarGap);
        Serial.println();

        byte foundFrontCarParkingSpotAndStopped_byte = (byte)foundFrontCarParkingSpotAndStopped;

        byte data_1[1] = {foundFrontCarParkingSpotAndStopped_byte};
        byte sndStat = CAN_Node.sendMsgBuf(foundParkingSpot_CANID, 1, data_1);
        if (sndStat == CAN_OK) {
          Serial.println("parkingSpotData_CANID: Message data_1 Sent Successfully!");
        } else {
          Serial.println("Error Sending Message data_1...");
        }

        uint16_t parkingLength_cm_uint16 = (uint16_t)parkingLength_cm;
        byte parkingLength_cm_uint16_H = (parkingLength_cm_uint16 & 0xFF << 8) >> 8;
        byte parkingLength_cm_uint16_L = parkingLength_cm_uint16 & 0xFF;

        uint16_t lengthBesidesFrontCar_cm_uint16 = (uint16_t)lengthBesidesFrontCar_cm;
        byte lengthBesidesFrontCar_cm_uint16_H = (lengthBesidesFrontCar_cm_uint16 & 0xFF << 8) >> 8;
        byte lengthBesidesFrontCar_cm_uint16_L = lengthBesidesFrontCar_cm_uint16 & 0xFF;

        uint8_t avg_FrontCarGap_cm_uint8 = (uint8_t)avg_FrontCarGap;
        byte avg_FrontCarGap_cm_uint8_byte = (byte)avg_FrontCarGap_cm_uint8;


        byte data_2[5] = {parkingLength_cm_uint16_H, parkingLength_cm_uint16_L, lengthBesidesFrontCar_cm_uint16_H, lengthBesidesFrontCar_cm_uint16_L, avg_FrontCarGap_cm_uint8_byte};
        byte sndStat_2 = CAN_Node.sendMsgBuf(parkingSpotData_CANID, 5, data_2);
        if (sndStat_2 == CAN_OK) {
          Serial.println("parkingSpotData_CANID: Message data_2 Sent Successfully!");
        } else {
          Serial.println("Error Sending Message data_1...");
        }

        resetParkingSpotData();
      }
      break;

    default:
      break;
  }

  Serial.println();
}

