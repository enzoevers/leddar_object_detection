
//#define SERIAL_MONITOR
//#define SERIAL_PLOTTER

// Use https://github.com/coryjfowler/MCP_CAN_lib
#include <mcp_can.h>
#include <SPI.h>

#include "soc/rtc.h"

#include "Leddar_object_detection.h"
#include "Parking_spot_detection.h"

MCP_CAN CAN0(5);     // Set CS to pin 5
const uint8_t can0_int = 2;

long unsigned int rxId;
unsigned char len = 0;
unsigned char rxBuf[8];
char msgString[128];                        // Array to store serial string


const int reset_CANID = 0x200;


bool processData_leddar_1 = false;
bool processData_leddar_2 = false;

void setup()
{
  Serial.begin(115200);

  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  //pinMode(can0_int, INPUT);
  // Check the MHZ value on the cristal that is on the CAN-SPI module!!!
  int can_init_res = CAN0.begin(MCP_ANY, CAN_1000KBPS, MCP_8MHZ);
  if (can_init_res == CAN_OK) Serial.println("MCP2515 Initialized Successfully!");
  else Serial.println("Error Initializing MCP2515...");

  CAN0.setMode(MCP_NORMAL);   // Change to normal mode to allow messages to be transmitted

  // send data:  ID = 0x100, Standard CAN Frame, Data length = 8 bytes, 'data' = array of data bytes to send
  byte sndData[8] = {0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

  byte sndStat = CAN0.sendMsgBuf(LeddarRx_1, 0, 8, sndData);
  Serial.print("sndStat: ");
  Serial.println(sndStat);
  if (sndStat == CAN_OK) {
    Serial.println("Message Sent Successfully!");
  } else {
    Serial.println("Error Sending Message...");
  }

  sndStat = CAN0.sendMsgBuf(LeddarRx_2, 0, 8, sndData);
  Serial.print("sndStat: ");
  Serial.println(sndStat);
  if (sndStat == CAN_OK) {
    Serial.println("Message Sent Successfully!");
  } else {
    Serial.println("Error Sending Message...");
  }

  delay(100);   // send data per 100ms

  initLeddarData();
  initObjectArray();
}

void loop()
{
  if (!digitalRead(can0_int))
  {
    CAN0.readMsgBuf(&rxId , &len, rxBuf);
    switch (rxId)
    {
      case 0:
        //Serial.println("ID: 0");
        break;
      case LeddarTx_1:
        parseDetectionData(rxId, len, rxBuf);
        break;
      case (LeddarTx_1 +1):
        processData_leddar_1 = true;
        break;
      case LeddarTx_2:
        parseDetectionData(rxId, len, rxBuf);
        break;
      case (LeddarTx_2 +1):
        processData_leddar_2 = true;
        //parseAdditionalInformation(rxId, len, rxBuf);
        break;
      case carSpeed_CANID:
        assignCarSpeed(len, rxBuf);
        break;
      case searchParkingSpot_CANID:
        setSearchParkingSpot(len, rxBuf);
        break;
      case reset_CANID:
        Serial.println("Received data on reset_CANID");
        if (rxBuf[0] == 1) {
          ESP.restart();
        }
        break;

      case distUltrasoneRequest_CANID:
        giveMeasuredDistToCar_CAN(CAN0, len, rxBuf);
        break;
      default:
        //Serial.print("Unkown ID: ");
        //Serial.println(rxId, HEX);
        //delay(1000);
        break;
    }

    if (processData_leddar_1 == true && processData_leddar_2 == true) {
      processXY();  // Process received data
      sendMIOsOverCAN(CAN0);

      parseAdditionalInformation(rxId, len, rxBuf);
      initLeddarData();
      processData_leddar_1 = false;
      processData_leddar_2 = false;
    }

    //giveMeasuredDistToCar_CAN(CAN0, len, rxBuf);
  }
}

void processXY() {
  if (getSearchParkingSpotFlag()) {
    parkingSpotMapping(CAN0);
  }
  //backMIO();
  backMIO_simple();
}

void sendXYArrayOverCAN() {

}
