frame = 10;

figure;
plot(X.data(idx.data(frame,:)==1,1,frame),X.data(idx.data(frame,:)==1,2,frame),'r.','MarkerSize',12)
hold on
plot(X.data(idx.data(frame,:)==2,1,frame),X.data(idx.data(frame,:)==2,2,frame),'b.','MarkerSize',12)
hold on
plot(X.data(idx.data(frame,:)==3,1,frame),X.data(idx.data(frame,:)==3,2,frame),'g.','MarkerSize',12)
plot(C.data(:,1,frame),C.data(:,2,frame),'kx',...
     'MarkerSize',15,'LineWidth',3) 
legend('Cluster 1','Cluster 2','Cluster 3','Centroids',...
       'Location','NW')
title 'Cluster Assignments and Centroids'
hold off