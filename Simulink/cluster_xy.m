
figure;

for i = 1:size(sorted_xy_data, 3)
    %c = clusterdata(dataset(:, :, i), 'Maxclust',3);
    
    Z = linkage(sorted_xy_data(:, :, i));
    c = cluster(Z, 'maxclust', 4);
    cutoff = median([Z(end-2, 3) Z(end-1, 3)]);
    dendrogram(Z, 'ColorThreshold', cutoff);
    %axis([1 32 0 10]);
    
    pause(0.00001);
end