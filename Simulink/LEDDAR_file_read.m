%%
% Read the data from the file
% Columns:
% | Time (ms) | Segment [0 15] | Amplitude [0 512] | Distance (m) |
% Status |
% The status column is not used because this also isn't present in the
% messages received over CAN.

filename = 'ATEAM_LEDDAR_EVAL-2019-02-21-13-50-40-692_CartonBoxOpenHall3_D100cm_W31cm_LeftRightLeft_100cm.txt';
fid = fopen(filename, 'r');
a = fscanf(fid, '%u %u %g %g %u', [5 inf]);
%a(1, :) = a(1, :) - a(1, 1); 
a(1, :) = 0:1:(size(a, 2)-1);
simin = a';
fclose(fid);

formatted_simin = format_record(simin);
%formatted_simin = formatted_simin(1:2, :);

%%
% Save data to a timeseries
ts = timeseries(formatted_simin(:,2:end),formatted_simin(:,1)); 
save('mySignals.mat','ts', '-v7.3')


