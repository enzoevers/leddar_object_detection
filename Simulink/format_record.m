function [formatted_file_data] = format_record(file_data)
%FORMAT_RECORD Summary of this function goes here
%   Detailed explanation goes here

col_in_time = 1;
col_in_segment = 2;
col_in_amplitude = 3;
col_in_distance = 4;
col_in_status = 5;

col_out_time = 1;
col_out_segment = 2;
col_out_amplitude = 3;
col_out_distance = 4;
col_out_segment1 = 5;
col_out_amplitude1 = 6;
col_out_distance1 = 7;
col_out_timestamp = 8;

% | time | segment | amplitude | distance | segment1 | amplitude1 | distance1 |

input_index = 1;
output_index = 1;

timestamp = 1;

% Go from m to cm
file_data(:, col_in_distance) = file_data(:, col_in_distance)*100;

for i = 1:size(file_data, 1)
    if(input_index <= size(file_data, 1))
        formatted_file_data(output_index, col_out_time) = (output_index-1) * 1;
        formatted_file_data(output_index, col_out_segment:col_out_distance) = file_data(input_index, col_in_segment:col_in_distance);
        formatted_file_data(output_index, col_out_timestamp) = 0;

        if(input_index+1 <= size(file_data, 1))
            if(file_data(input_index+1, col_in_segment) >= file_data(input_index, col_in_segment))
                input_index = input_index+1;
                formatted_file_data(output_index, col_out_segment1:col_out_distance1) = file_data(input_index, col_in_segment:col_in_distance);
            else
                formatted_file_data(output_index, col_out_segment1:col_out_distance1) = [0, 0, 0];
            end

            if(file_data(input_index+1, col_in_segment) < file_data(input_index, col_in_segment))
                output_index = output_index + 1;
                formatted_file_data(output_index, col_out_time) = (output_index-1) * 1;
                formatted_file_data(output_index, col_out_segment:col_out_distance1) = -1;
                formatted_file_data(output_index, col_out_timestamp) = timestamp;
            end
        end

        output_index = output_index + 1;
        input_index = input_index+1;
    end
end

end


