%%
%{
coordinates = [ 3   , -10; 
                2.8 , -8; 
                3   , -6; 
                1   , -4; 
                1.3   , -2; 
                1   , 0; 
                6   , 2; 
                5.5   , 4; 
                3   , 6; 
                3   , 8; 
                9   , 10];
 %}
coordinates = [ 3   , 1; 
                2.8 , 2; 
                3   , 3; 
                1   , 4; 
                1.3   , 5;
                1   , 6; 
                6   , 7; 
                5.5   , 8; 
                3   , 9; 
                3   , 10; 
                9   , 11    ];

            
%%       
cutoff = 1.3;
xy = coordinates;
invalid_val = 65536;

cluster_index = clusterdata(xy, ...
                            'Criterion', 'distance', ...
                            'Cutoff', cutoff);
                        
my_cluster_index = my_clusterdata(xy, cutoff);
            
num_clusters = max(cluster_index);

% With two LEDDARs the maximum number of coordinates is 32
closest_cluster_points = zeros(32, 2);
closest_cluster_points(:) = invalid_val;

% Get the closest coordinate of each cluster
for current_cluster = 1:num_clusters
    cluster_indicis = find(cluster_index == current_cluster);
    cluster_data = xy(cluster_indicis, :);
    
    for i = 1:size(cluster_data, 1)
        if cluster_data(i, 1) < closest_cluster_points(current_cluster, 1)
            closest_cluster_points(current_cluster, :) = cluster_data(i, :);
        end
    end
end

% Sort the closts coordinates of each cluster
x = closest_cluster_points(:, 1);
y = closest_cluster_points(:, 2);

[closest_cluster_points(:, 1), sorted_index] = sort(x);
closest_cluster_points(:, 2) = y(sorted_index);

% Assign the closest points
closest_point_1 = closest_cluster_points(1, :);
closest_point_2 = closest_cluster_points(2, :);
closest_point_3 = closest_cluster_points(3, :);
closest_point_4 = closest_cluster_points(4, :);

% Plot
figure;
scatter(xy(:,2), xy(:,1));

Z = linkage(xy);
c = cluster(Z, 'Criterion', 'distance', 'Cutoff', cutoff);
%cutoff = median([Z(end-2, 3) Z(end-1, 3)]);

figure;
dendrogram(Z, 'ColorThreshold', cutoff);