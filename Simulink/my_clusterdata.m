function [cluster_indicis] = my_clusterdata(sorted_xy, cutoff)
% The input data (xy) should be sorted based on the y data.
% This means having data that goes from th left to right.

num_points = size(sorted_xy, 1);
cluster_indicis = zeros(num_points, 1);

cur_cluster = 1;
cluster_indicis(1) = cur_cluster;

% The minus one is because each point is compared with the previous one.
for i = 2:num_points
    cur_point = sorted_xy(i, :);
    prev_point = sorted_xy(i-1, :);
    
    delta_x = cur_point(1) - prev_point(1);
    delta_y = cur_point(2) - prev_point(2);
    
    euclid_dist = sqrt(delta_x^2 + delta_y^2);
    
    if euclid_dist < cutoff
        cluster_indicis(i) = cur_cluster;
    else
        cur_cluster = cur_cluster + 1;
        cluster_indicis(i) = cur_cluster;
    end
end

end